import { observable, action, computed, runInAction } from 'mobx'
import { CovidData } from '../models/cases';
import { AppEnum } from '../enums/app.enums';


export class PatientsStore {
  @observable casesList:Array<CovidData> = [];
  @observable filteredCasesList:any={}
  @observable SelectedState: string = "";



  @action
  setAllCovidCases(data:CovidData[]) {
    debugger
    this.casesList=data;
    this.SelectedState=data[0].state
    this.filteredCasesList=data[0];
  }

  @action
  setSelectedState(state: string) {
    this.SelectedState=state;
      this.filteredCasesList=this.casesList.find((s)=>{
        return s.state==state
      });
  }

 
}

export const patientsStore = new PatientsStore()
