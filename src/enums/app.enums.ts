export enum AppEnum {

    noDescriptionFound = "No description found!",
    appHeading = "Covid Patients",
    apiUrl = "https://api.covidindiatracker.com/state_data.json",
    noPatientsFound = "No patients found!"


}