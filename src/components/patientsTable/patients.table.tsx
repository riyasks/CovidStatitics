// import * as React from "react"
// import { AppEnum } from "../../enums/app.enums"
// import { UserStore } from "../../store/users.store"



// interface UserModel {

//     userStore: UserStore

// }



// const PatientsTable:  React.SFC<UserModel> = (props) => {
//     const ss=props.userStore.casesList.filter((data)=>{
//         return data.state===props.userStore.SelectedState
//     })
//     return (
//         <div id="main">
//           Length  {ss}
// <table className="table table-striped">
//     <thead>
//       <tr>
//         <th>Firstname</th>
//         <th>Lastname</th>
//         <th>Email</th>
//       </tr>
//     </thead>
//     <tbody>
//       <tr>
//         <td>John</td>
//         <td>Doe</td>
//         <td>john@example.com</td>
//       </tr>
//       <tr>
//         <td>Mary</td>
//         <td>Moe</td>
//         <td>mary@example.com</td>
//       </tr>
//       <tr>
//         <td>July</td>
//         <td>Dooley</td>
//         <td>july@example.com</td>
//       </tr>
//     </tbody>
//   </table>

//             {/* <div className="fof">
//                 <h1>{AppEnum.noPatientsFound}</h1>
//                 {props.userStore.SelectedState}
//             </div> */}
//         </div>
//     )
// }

// export default PatientsTable


import React, { Fragment } from 'react';
import { Component } from 'react';
import { observer, inject } from 'mobx-react';

import NoPatients from '../noPatientsFound/nopatientsfound'
import { AppEnum } from '../../enums/app.enums';
import { UserProps } from '../../models/props';
import { CovidData } from '../../models/cases';

@inject('patientsStore')
@observer
export class PatientsTable extends Component<UserProps, {}> {

  constructor(props) {
    super(props);
  }

  render() {
    let _case: CovidData = this.props.patientsStore.filteredCasesList

    return (

      <Fragment>
        <h6><div>Tabular({this.props.patientsStore.SelectedState})</div></h6>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>State</th>
              <th>Recovered</th>
              <th>Active Cases</th>
              <th>Death</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{_case ? _case.state : ""}</td>
              <td>{_case ? _case.recovered : ""}</td>
              <td>{_case ? _case.active : ""}</td>
              <td>{_case ? _case.deaths : ""}</td>
            </tr>
          </tbody>
        </table>
      </Fragment>
    )
  }
}

