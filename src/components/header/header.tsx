import * as React from "react"
import { inject, observer } from "mobx-react";
import { AppEnum } from "../../enums/app.enums";
import { Fragment } from "react";
import { UserProps } from "../../models/props";




const AppHeader: React.SFC<UserProps> = (props) => {

    // props.patientsStore.getAllCases()
    // const [user, setUser] = React.useState(new User());
    const filterList = event => {


        let value = event.target.value//.toLowerCase();


        props.patientsStore.setSelectedState(value);


    }


    const optionTemplate = props.patientsStore.casesList.map(v => (
        <option value={v.state} key={v.id}>{v.state}</option>
    ));



    return (

        <Fragment>
            <nav className="navbar navbar-expand-md navbar-dark bg-success bg-dark sticky-top">
                <a className="navbar-brand" id="heading">{AppEnum.appHeading}</a>
                <div className="col-sm-3 col-md-3 pull-right">
                    <form className="navbar-form" role="search">
                        <div className="input-group">
                            <select value={props.patientsStore.SelectedState} onChange={(e) => { filterList(e) }}>
                                {optionTemplate}
                            </select>

                        </div>
                    </form>

                </div>


            </nav>



        </Fragment>
    )
}

export default inject('patientsStore')(observer(AppHeader))