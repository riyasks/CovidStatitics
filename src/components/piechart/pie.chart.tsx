

import React, { Fragment } from 'react';
import { Component } from 'react';
import { observer, inject } from 'mobx-react';

import { UserProps } from '../../models/props';
import { CovidData } from '../../models/cases';

import Plot from 'react-plotly.js';
@inject('patientsStore')
@observer
export class PatientsPieChart extends Component<UserProps, {}> {

  constructor(props) {
    super(props);
  }

  render() {
   
    let _case: CovidData = this.props.patientsStore.filteredCasesList
    return (

      <Fragment>
          <h6><div>Pie chart({this.props.patientsStore.SelectedState})</div></h6>    
          <Plot
        data={ [{
            values: [_case.recovered, _case.active, _case.deaths],
            labels: ['Recovered', 'Active Cases', 'Death'],
            type: 'pie'
          }]}
        layout={{
            height: 400,
            width: 500
          }}
      />
       <div id="pie"></div>
      </Fragment>
    )
  }
}

