import React, { Fragment } from 'react';
import { Component } from 'react';
import { observer, inject } from 'mobx-react';


import NoPatients from '../noPatientsFound/nopatientsfound'
import { AppEnum } from '../../enums/app.enums';
import { UserProps } from '../../models/props';
import { PatientsTable } from "../patientsTable/patients.table"
import { PatientsPieChart } from "../piechart/pie.chart"
import { HeatMap } from "../heatMap/heatMap"
import { CovidData } from '../../models/cases';
@inject('patientsStore')
@observer
export class CovidDashboard extends Component<UserProps, {}> {

    constructor(props) {
        super(props);
        this.getCovidData();

    }

    getCovidData() {
        return new Promise<CovidData[]>((resolve, reject) => {
            fetch(AppEnum.apiUrl).then(response => response.json()).then((data) => {
                this.props.patientsStore.setAllCovidCases(data);
                resolve(data);
            }).catch((err) => {
                reject(err)
            })
        })
    }

    render() {

        return (
            <Fragment>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-6 pull-left colStyle">
                            <PatientsTable />
                        </div>
                        <div className="col-sm-6 pull-right colStyle">
                            <PatientsPieChart />
                        </div></div>
                    <div className="row">
                        <div className="col-sm-6 pull-left colStyle">
                            <HeatMap geoData={this.props.patientsStore.casesList} />
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

