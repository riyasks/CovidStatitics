

import React, { Fragment } from 'react';
import { Component } from 'react';
import { observer, inject } from 'mobx-react';

import { UserProps, HeatProps } from '../../models/props';

import * as am4core from "@amcharts/amcharts4/core";

import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_india2019High from "@amcharts/amcharts4-geodata/india2019High";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
@inject('patientsStore')
@observer
export class HeatMap extends Component<HeatProps, {}> {
    PROJECTION_CONFIG: { scale: number; center: number[]; };
    caseType: string = "active"
    data: any = [
        {
            id: "IN-JK",
            value: 0
        },
        {
            id: "IN-MH",
            value: 0
        },
        {
            id: "IN-UP",
            value: 0
        },
        {
            id: "US-AR",
            value: 0
        },
        {
            id: "IN-RJ",
            value: 0
        },
        {
            id: "IN-AP",
            value: 0
        },
        {
            id: "IN-MP",
            value: 0
        },
        {
            id: "IN-TN",
            value: 0
        },
        {
            id: "IN-JH",
            value: 0
        },
        {
            id: "IN-WB",
            value: 0
        },
        {
            id: "IN-GJ",
            value: 0
        },
        {
            id: "IN-BR",
            value: 0
        },
        {
            id: "IN-TG",
            value: 0
        },
        {
            id: "IN-GA",
            value: 0
        },
        {
            id: "IN-DN",
            value: 0
        },
        {
            id: "IN-DL",
            value: 0
        },
        {
            id: "IN-DD",
            value: 0
        },
        {
            id: "IN-CH",
            value: 0
        },
        {
            id: "IN-CT",
            value: 0
        },
        {
            id: "IN-AS",
            value: 0
        },
        {
            id: "IN-AR",
            value: 0
        },
        {
            id: "IN-AN",
            value: 0
        },
        {
            id: "IN-KA",
            value: 0
        },
        {
            id: "IN-KL",
            value: 0
        },
        {
            id: "IN-OR",
            value: 0
        },
        {
            id: "IN-SK",
            value: 0
        },
        {
            id: "IN-HP",
            value: 0
        },
        {
            id: "IN-PB",
            value: 0
        },
        {
            id: "IN-HR",
            value: 0
        },
        {
            id: "IN-UT",
            value: 0
        },
        {
            id: "IN-LK",
            value: 0
        },
        {
            id: "IN-MN",
            value: 0
        },
        {
            id: "IN-TR",
            value: 0
        },
        {
            id: "IN-MZ",
            value: 0
        },
        {
            id: "IN-NL",
            value: 0
        },
        {
            id: "IN-ML",
            value: 0
        }
    ];
    constructor(props) {
        super(props);
        this.PROJECTION_CONFIG = {
            scale: 350,
            center: [78.9629, 22.5937]
        };




    }



    componentWillReceiveProps() {

        this.setData(this.caseType)
    }

    setData(caseType) {
        this.caseType = caseType;
        for (let index = 0; index < this.data.length; index++) {
            const geo = this.data[index];
            for (let z = 0; z < this.props.patientsStore.casesList.length; z++) {
                const _case = this.props.patientsStore.casesList[z];
                if (geo.id == _case.id) {
                    this.data[index].value = caseType === "active" ? _case.active :
                        caseType === "confirmed" ? _case.confirmed :
                            _case.recovered;

                }
            }

        }
        this.updateChart()
    }

    updateChart() {


        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create map instance
        var chart = am4core.create("chartdiv", am4maps.MapChart);

        // Set map definition
        chart.geodata = am4geodata_india2019High;



        // Create map polygon series
        var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

        //Set min/max fill color for each area
        polygonSeries.heatRules.push({
            property: "fill",
            target: polygonSeries.mapPolygons.template,
            min: chart.colors.getIndex(1).brighten(1),
            max: chart.colors.getIndex(1).brighten(-0.8)
        });

        // Make map load polygon data (state shapes and names) from GeoJSON
        polygonSeries.useGeodata = true;
        // polygonSeries.dataFields.value="active";

        // Set heatmap values for each state
        polygonSeries.data = this.data



        // Configure series tooltip
        var polygonTemplate = polygonSeries.mapPolygons.template;
        polygonTemplate.tooltipText = this.caseType + " : {value}";
        polygonTemplate.nonScalingStroke = true;
        polygonTemplate.strokeWidth = 0.5;


        // Create hover state and set alternative fill color
        var hs = polygonTemplate.states.create("hover");
        hs.properties.fill = am4core.color("#3c5bdc");



    }


    render() {



        return (

            <Fragment>
                <h6><div>Heat Map India Covid-19</div></h6>
                <div className="btn-group">
                    <button type="button" className="btn btn-primary active" onClick={() => {
                        this.caseType = "active"
                        this.setData(this.caseType)
                    }}>Active</button>
                    <button type="button" className="btn btn-primary" onClick={() => {
                        this.caseType = "confirmed"
                        this.setData(this.caseType)
                    }}>confirmed</button>
                    <button type="button" className="btn btn-primary" onClick={() => {
                        this.caseType = "death"
                        this.setData(this.caseType)
                    }}>death</button>
                </div>
                <div id="chartdiv"></div>
            </Fragment>
        )
    }
}

