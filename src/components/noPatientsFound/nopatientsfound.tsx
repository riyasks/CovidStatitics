import * as React from "react"
import { AppEnum } from "../../enums/app.enums"

const NoPatients: React.SFC = () => {
    return (
        <div id="main">
            <div className="fof">
                <h1>{AppEnum.noPatientsFound}</h1>
            </div>
        </div>
    )
}

export default NoPatients