import React, { Fragment } from 'react'
import { Component } from 'react'
import { Provider } from 'mobx-react'
import { CovidDashboard } from './components/covidPatientsList/covid.patients.list'
import { PatientsStore } from './store/patients.store'
import AppHeader from './components/header/header'


export class App extends Component {
  //Inject the patientsStore 
  private patientsStore: PatientsStore = new PatientsStore()

  render() {
    return (
      <Provider patientsStore={this.patientsStore}>
       <AppHeader/>
        <CovidDashboard  />
      </Provider>
    )
  }
}
