
export interface DistrictData {
    id: string;
    state?: any;
    name: string;
    confirmed: number;
    recovered?: any;
    deaths?: any;
    oldConfirmed: number;
    oldRecovered?: any;
    oldDeaths?: any;
    zone: string;
}

export class CovidData {
    id: string;
    state: string;
    active: number;
    confirmed: number;
    recovered: number;
    deaths: number;
    aChanges: number;
    cChanges: number;
    rChanges: number;
    dChanges: number;
    districtData: DistrictData[];
    achanges: number;
    dchanges: number;
    rchanges: number;
    cchanges: number;
}

